output "wp_server_ip" {
  value = aws_instance.wp_app_server.public_ip
}
output "wp_db_endpoint" {
  value = aws_db_instance.wp_db_server.address
}