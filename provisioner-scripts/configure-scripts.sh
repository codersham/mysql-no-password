#!/bin/bash
sed 's/<db_endpoint>/'$1'/1' ./provisioner-scripts/create-user_template > ./provisioner-scripts/temp

mv -f ./provisioner-scripts/temp ./provisioner-scripts/create-user.sh

sed 's/<db_user_id>/'$2'/1' ./provisioner-scripts/create-user.sh > ./provisioner-scripts/temp

mv -f ./provisioner-scripts/temp ./provisioner-scripts/create-user.sh

sed 's/<db_user_password>/'$3'/1' ./provisioner-scripts/create-user.sh > ./provisioner-scripts/temp

mv -f ./provisioner-scripts/temp ./provisioner-scripts/create-user.sh

sed 's/<db_endpoint>/'$4'/g' ./provisioner-scripts/connect-no-password_template > ./provisioner-scripts/temp

mv -f ./provisioner-scripts/temp ./provisioner-scripts/connect-no-password.sh