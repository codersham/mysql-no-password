# RDS Subnet Group
resource "aws_db_subnet_group" "wp_rds_subnetgroup" {
  name       = "wp_rds_subnetgroup"
  subnet_ids = [module.wp_network.subnet_ids[1], module.wp_network.subnet_ids[2]]

  tags = {
    Name = var.wp_project_name
  }
}

# Create the Wordpress MySql Server
resource "aws_db_instance" "wp_db_server" {
  allocated_storage = 20
  engine            = "mysql"
  instance_class    = "db.t2.micro"
  //name                                = var.db_name
  username                            = var.wp_username
  password                            = var.wp_password
  iam_database_authentication_enabled = true
  db_subnet_group_name                = aws_db_subnet_group.wp_rds_subnetgroup.name
  vpc_security_group_ids              = [module.wp_sg.mysql_only]
  backup_retention_period             = 0
  multi_az                            = false

  tags = {
    Name = var.wp_project_name
  }
}