variable "wp_aws_profile" {
  type = string
}
variable "wp_aws_region" {
  type = string
}

variable "wp_project_name" {
  type = string
}
data "aws_ssm_parameter" "wp_ami" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}
variable "ec2_public_key_path" {
  type = string
}
variable "ec2_key_name" {
  type = string
}
variable "ec2_private_key_path" {
  type = string
}
variable "wp_instance_type" {
  type    = string
  default = "t2.micro"
}
variable "wp_username" {
  type = string
}
variable "wp_password" {
  type = string
}
# variable "db_name" {
#   type = string
# }
variable "number_of_subnets" {
  type = number
}