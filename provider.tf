terraform {
  required_version = ">=0.13"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.0"
    }
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
  # backend "remote" {
  #   organization = "IaC-Pipeline"

  #   workspaces {
  #     name = "wordpress"
  #   }
  # }

}

provider "aws" {
  profile = var.wp_aws_profile
  region  = var.wp_aws_region
}