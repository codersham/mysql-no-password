resource "aws_iam_role" "ec2_rds_role" {
  name = "ec2_rds_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name = var.wp_project_name
  }
}

resource "aws_iam_role_policy" "ec2_rds_policy" {
  name = "ec2_rds_policy"
  role = aws_iam_role.ec2_rds_role.id

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
      "Action":[ "rds:*"],
       "Resource": "*",
       "Effect": "Allow"
    },
      {
        "Effect": "Allow",
        "Action": [
            "rds-db:connect"
        ],
        "Resource": [
            "arn:aws:rds-db:us-east-1:478481990230:dbuser:${aws_db_instance.wp_db_server.resource_id}/tiger",
            "arn:aws:rds-db:us-east-1:478481990230:dbuser:${aws_db_instance.wp_db_server.resource_id}/scott"
        ]
    }
    ]
  }
  EOF

  depends_on = [aws_db_instance.wp_db_server]
}