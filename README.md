# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is to demonstrate RDS MySql can be connected password-lessly, with an IAM role


### How do I get set up? ###

* Cloning this repo will download necessary configuration files
* It will require the following files: 
* 		1. terraform.tfvars to inlude definition of all variables in variables.tf file
*		2. A pair of key

### What do I after set up? ###

* SSH into ec2 using the private key
* execute /home/ec2-user//provisioner-scripts/create-user.sh => This will create user without password
* execute /home/ec2-user//provisioner-scripts/connect-no-password.sh => This will genearte temporary token and connect to the user without password