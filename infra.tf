module "wp_network" {
  #source       = "../../modules/network"
  source       = "git::https://codersham@bitbucket.org/codersham/networks.git"
  project_name = var.wp_project_name
  subnet_count = var.number_of_subnets
}
module "wp_sg" {
  #source     = "../../modules/security_groups"
  source     = "git::https://codersham@bitbucket.org/codersham/security_groups.git"
  aws_vpc_id = module.wp_network.my_vpc_id
}